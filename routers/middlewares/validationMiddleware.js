const Joi = require('joi');
const {BadRequestError} = require('../../models/errorModel');

const {ROLE, TRUCK_TYPES_LIST} = require('../../globalConstants');

const regExps = {
  role: new RegExp(`${ROLE.shipper}|${ROLE.driver}`),
  password: new RegExp('^[a-zA-Z0-9]{6,30}$'),
  truckType: new RegExp(`${TRUCK_TYPES_LIST
      .map((truck) => truck.type)
      .join('|')
  }`),
};

const validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi
        .string()
        .email()
        .required(),
    password: Joi
        .string()
        .required()
        .pattern(regExps.password),
    role: Joi
        .string()
        .required()
        .pattern(regExps.role),
  });

  await validateSchema(schema, req.body);

  next();
};

const validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi
        .string()
        .required()
        .pattern(regExps.password),
    newPassword: Joi
        .string()
        .required()
        .pattern(regExps.password),
  });

  await validateSchema(schema, req);
  next();
};

const validateUserTruck = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi
        .string()
        .required()
        .pattern(regExps.truckType),
  });
  await validateSchema(schema, req.body);
  next();
};

const validateUserLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi
        .string()
        .required(),
    payload: Joi
        .number()
        .greater(0)
        .required(),
    pickup_address: Joi
        .string()
        .required(),
    delivery_address: Joi
        .string()
        .required(),
    dimensions: Joi.object({
      width: Joi
          .number()
          .greater(0)
          .required(),
      length: Joi
          .number()
          .greater(0)
          .required(),
      height: Joi
          .number()
          .greater(0)
          .required(),
    }),
  });

  await validateSchema(schema, req.body);
  next();
};

/**
 * Asynchronous schema validator
 * @param {Object} schema - Joi schema to be validated
 * @param {Object} body - client request body
 * */
async function validateSchema(schema, body) {
  try {
    await schema.validateAsync(body);
  } catch (e) {
    throw new BadRequestError(e.message);
  }
}

module.exports = {
  validateRegistration,
  validateChangePassword,
  validateUserTruck,
  validateUserLoad,
};
