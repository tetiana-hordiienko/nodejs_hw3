const {UnathorizedError} = require('../../models/errorModel');
const {isDriver} = require('../helpers/checksHelper');

const ensureDriverRole = (req, res, next) => {
  if (!isDriver(req.user)) {
    throw new UnathorizedError('No rights!');
  }
  next();
};

const ensureShipperRole = (req, res, next) => {
  if (isDriver(req.user)) {
    throw new UnathorizedError('No rights!');
  }
  next();
};

module.exports = {
  ensureDriverRole,
  ensureShipperRole,
};
