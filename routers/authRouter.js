const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers/asyncHelper');
const {checkCandidate} = require('../models/dao/daoUser');
const {
  createProfile,
  login,
  forgotPassword,
} = require('../controllers/authController');
const {validateRegistration} = require('./middlewares/validationMiddleware');

router.post(
    '/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(checkCandidate),
    asyncWrapper(createProfile),
);
router.post(
    '/login',
    asyncWrapper(login),
);
router.post(
    '/forgot_password',
    asyncWrapper(forgotPassword),
);

module.exports = router;
