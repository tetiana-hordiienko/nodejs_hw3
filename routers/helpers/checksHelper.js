const {User} = require('../../models/userModel');
const {Truck} = require('../../models/truckModel');
const {ROLE} = require('../../globalConstants');

const userExists = async (fieldValue) =>
  await User.exists(fieldValue);

const isDriver = (user) => user.role === ROLE.driver;

const isUserTruck = async (truckId, userId) => {
  const truck = await Truck.findById(truckId);

  if (!truck) {
    return false;
  }

  return userId === truck['created_by'];
};

const isTruckAssigned = (truck) => truck['assigned_to'];

const truckAssignedToUser = (truck, user) =>
  truck['assigned_to'] === user['_id'];

const userHasAssignedTruck = (trucks) => {
  for (const truck of trucks) {
    if (truck['assigned_to']) {
      return truck;
    }
  }
  return false;
};

module.exports = {
  userExists,
  isDriver,
  isUserTruck,
  isTruckAssigned,
  truckAssignedToUser,
  userHasAssignedTruck,
};
