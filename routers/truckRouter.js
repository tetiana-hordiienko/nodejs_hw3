const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers/asyncHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateUserTruck} = require(
    '../routers/middlewares/validationMiddleware',
);
const {ensureDriverRole} = require(
    './middlewares/ensureUserRoleMiddleware',
);
const {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTruckById,
  assignUserTruckById,
  deleteUserTruckById,
} = require('../controllers/truckController');

router.use(asyncWrapper(authMiddleware), ensureDriverRole);

router.get('/', asyncWrapper(getUserTrucks));

router.post('/',
    asyncWrapper(validateUserTruck),
    asyncWrapper(addUserTruck),
);

router.get('/:id', asyncWrapper(getUserTruckById));

router.put(
    '/:id',
    asyncWrapper(validateUserTruck),
    asyncWrapper(updateUserTruckById),
);

router.delete('/:id', asyncWrapper(deleteUserTruckById));

router.post('/:id/assign', asyncWrapper(assignUserTruckById));

module.exports = router;
