const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers/asyncHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateChangePassword} = require('./middlewares/validationMiddleware');
const {
  getProfileInfo,
  changeProfilePassword,
  deleteProfile,
} = require('../controllers/userController');

router.use(asyncWrapper(authMiddleware));

router.get(
    '/',
    asyncWrapper(getProfileInfo),
);
router.patch(
    '/password',
    asyncWrapper(validateChangePassword),
    asyncWrapper(changeProfilePassword),
);
router.delete(
    '/',
    asyncWrapper(deleteProfile),
);

module.exports = router;
