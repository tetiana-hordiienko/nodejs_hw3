const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers/asyncHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateUserLoad} =
    require('../routers/middlewares/validationMiddleware');
const {
  ensureShipperRole,
  ensureDriverRole,
} = require('./middlewares/ensureUserRoleMiddleware');
const {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  postUserLoadById,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  getUserLoadShippingDetailsById,
} = require('../controllers/loadController');

router.use(asyncWrapper(authMiddleware));

router.get('/', asyncWrapper(getUserLoads));
router.post(
    '/',
    ensureShipperRole,
    asyncWrapper(validateUserLoad),
    asyncWrapper(addUserLoad),
);
router.get(
    '/active',
    ensureDriverRole,
    asyncWrapper(getUserActiveLoad),
);

router.patch(
    '/active/state',
    ensureDriverRole,
    asyncWrapper(iterateLoadState),
);

router.get(
    '/:id',
    asyncWrapper(getUserLoadById),
);

router.put(
    '/:id',
    ensureShipperRole,
    asyncWrapper(updateUserLoadById),
);

router.delete(
    '/:id',
    ensureShipperRole,
    asyncWrapper(deleteUserLoadById),
);

router.get(
    '/:id/shipping_info',
    ensureShipperRole,
    asyncWrapper(getUserLoadShippingDetailsById),
);

router.post(
    '/:id/post',
    ensureShipperRole,
    asyncWrapper(postUserLoadById),
);

module.exports = router;
