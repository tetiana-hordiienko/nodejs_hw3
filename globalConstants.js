const ROLE = {
  shipper: 'SHIPPER',
  driver: 'DRIVER',
};

const LOAD_STATUSES = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

const TRUCK_STATUSES = ['IS', 'OL'];

const LOAD_STATES = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to Delivery',
  'Arrived to Delivery',
];

const findStatusIndex = (statusToFind, statusesList) => {
  return statusesList.findIndex((status) => status === statusToFind);
};

const iterateState = (stateList, oldState) => {
  let curInd = findStatusIndex(oldState, stateList);
  return curInd < stateList.length - 1
         ? stateList[++curInd]
         : stateList[curInd];
};

const TRUCK_TYPES_LIST = [
  {
    type: 'SPRINTER',
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    dimensions: {
      width: 700,
      length: 350,
      height: 200,
    },
    payload: 4000,
  },
];

module.exports = {
  ROLE,
  TRUCK_TYPES_LIST,
  LOAD_STATUSES,
  TRUCK_STATUSES,
  LOAD_STATES,
  iterateState,
};
