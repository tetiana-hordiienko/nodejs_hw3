const {Load} = require('../loadModel');
const {BadRequestError} = require('../errorModel');

const getAllLoads = async (filter, requestOptions) => {
  return (await Load.find(
      filter,
      ['-__v'],
      requestOptions,
  ));
};

const addLoad = async (userId, reqBody) => {
  const {
    name, payload, pickup_address, delivery_address,
    dimensions: {
      width, length, height,
    },
  } = reqBody;
  const load = new Load({
    created_by: userId,
    name, payload, pickup_address, delivery_address,
    dimensions: {
      width, length, height,
    },
  });
  await load.save();
};

const findLoadById = async (_id) => {
  const load = await Load.findById(_id);

  if (!load) {
    throw new BadRequestError('Load not found!');
  }

  return load;
};

const updateLoad = async (load, updates, logMsgs) => {
  if (updates) {
    for (const field in updates) {
      load[field] = updates[field];
    }
    addLoadLog(load, logMsgs);
    await load.save();
  }
};

const addLoadLog = (load, msgs) => {
  for (const msg of msgs) {
    load.logs.push({
      message: msg,
      time: Date.now(),
    });
  }
};

const findActiveLoad = async (userId) => {
  const filter = {
    assigned_to: userId,
    status: 'ASSIGNED',
  };

  return (await Load.findOne(filter));
};

const findAndPostLoad = async (userId, loadId, status) => {
  const load = await findLoadById(loadId);

  ensureUserLoad(load, userId);

  if (load.status !== 'NEW') {
    throw new BadRequestError('Cannot post this load');
  }

  const update = {status};

  load.status = status;

  await updateLoad(load, update, ['Load status changed to POSTED']);

  return load;
};

const ensureUserLoad = (load, userId) => {
  if (load['created_by'] !== userId) {
    throw new BadRequestError('No rights to interact with the load!');
  }
};

const ensureNewLoadStatus = (load) => {
  if (load.status !== 'NEW') {
    throw new BadRequestError('Only possible to change load with status NEW');
  }
};

const deleteLoadById = async (_id) => {
  await Load.deleteOne({_id});
}

module.exports = {
  getAllLoads,
  addLoad,
  updateLoad,
  findLoadById,
  findAndPostLoad,
  addLoadLog,
  findActiveLoad,
  ensureNewLoadStatus,
  ensureUserLoad,
  deleteLoadById,
};
