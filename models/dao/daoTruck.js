const {Truck} = require('../truckModel');
const {DatabaseError, BadRequestError} = require('../errorModel');
const {TRUCK_STATUSES, TRUCK_TYPES_LIST} = require('../../globalConstants');
const {isUserTruck} = require('../../routers/helpers/checksHelper');

const getAllTrucks = async (user) => {
  try {
    return (await Truck.find(
        {created_by: user['_id']},
        ['-__v'],
    )
    );
  } catch (e) {
    throw new DatabaseError(`Database read error`);
  }
};

const findTruckById = async (_id, userId) => {
  if (!(await isUserTruck(_id, userId))) {
    throw new BadRequestError(`Truck not found or not possible to see it!`);
  }

  return (await Truck.findById(_id));
};

const findTruckByDriverId = async (driverId) => {
  return (await Truck.findOne({
    created_by: driverId,
    status: 'OL',
  }));
};

const addTruck = async (userId, truckType) => {
  const truck = new Truck({
    created_by: userId,
    type: truckType,
  });

  try {
    await truck.save();
  } catch (e) {
    throw new DatabaseError(`Something went wrong! Truck haven't been saved!`);
  }
};

const changeTruckInfo = async (_id, newValue) => {
  await Truck.findById(
      _id,
      (err, truck) => {
        if (err) {
          throw new DatabaseError('Update failed!');
        }
        truck.type = newValue;
        truck.save();
      },
  );
};

const assignTruckToUser = async (truckId, userId) => {
  await Truck.findById(
      truckId,
      (err, truck) => {
        if (err) {
          throw new DatabaseError('Assigned truck failed!');
        }
        truck.assigned_to = userId;
        truck.save();
      },
  );
};

const deleteTruckById = async (_id) => {
  try {
    await Truck.deleteOne({_id});
  } catch (e) {
    throw new DatabaseError(`Truck haven't been deleted!`);
  }
};

const findSuitableTruck = async (load) => {
  const {payload: loadPayload, dimensions: loadDimensions} = load;

  const checkTruckDimensions = (loadDimensions, truckDimensions) => {
    const {width: lWidth, length: lLength, height: lHeight} = loadDimensions;
    const {width: tWidth, length: tLength, height: tHeight} = truckDimensions;
    return +lWidth <= +tWidth && +lLength <= +tLength && +lHeight <= +tHeight;
  };

  const truckPayloads = TRUCK_TYPES_LIST
      .map((truck) => truck.payload)
      .sort((a, b) => a - b);

  for (let i = 0; i < truckPayloads.length; i++) {
    const curTruckPayload = truckPayloads[i];
    if (loadPayload <= curTruckPayload) {
      const truck = TRUCK_TYPES_LIST.find((t) => t.payload === curTruckPayload);
      if (checkTruckDimensions(loadDimensions, truck.dimensions)) {
        return truck.type;
      }
    }
  }
};

const updateTruck = async (truck, updates) => {
    for (const field in updates) {
      truck[field] = updates[field];
    }
    await truck.save();
};

const findTruckToShip = async (truckType) => {
  const truck = await Truck.findOneAndUpdate(
      {
        status: TRUCK_STATUSES[0],
        assigned_to: {$ne: null},
        type: truckType,
      },
      {status: 'OL'},
  );

  if (!truck) {
    return null;
  }

  return truck;
};

module.exports = {
  getAllTrucks,
  addTruck,
  findTruckById,
  changeTruckInfo,
  assignTruckToUser,
  deleteTruckById,
  findTruckToShip,
  findSuitableTruck,
  updateTruck,
  findTruckByDriverId,
};
