const {User} = require('../userModel');
const {BadRequestError, DatabaseError} = require('../../models/errorModel');
const {userExists} = require('../../routers/helpers/checksHelper');

const findUserByEmail = async (email, msg = 'No user found') => {
  if (!(await userExists({email}))) {
    throw new BadRequestError(msg);
  }
  return (await User.findOne({email}));
};

const findUserById = async (_id) => {
  if (!(await userExists({_id}))) {
    throw new BadRequestError('No user found');
  }
  return (await User.findById(_id));
};

const checkCandidate = async (req, res, next) => {
  const {email} = req.body;
  if (await userExists({email})) {
    throw new BadRequestError(`User ${email} already exists!`);
  }
  next();
};

const deleteUserById = async (_id) => {
  try {
    await User.deleteOne({_id});
  } catch (e) {
    throw new DatabaseError(`User profile haven't been deleted!`);
  }
};

module.exports = {
  findUserByEmail,
  checkCandidate,
  findUserById,
  deleteUserById,
};
