const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: String,
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = {
  Truck,
};
