/**
 * Class for getting custom name for extended classes
 * @extends Error
 */
class ApplicationError extends Error {
  /**
   * Get the custom class name
   * @return {string} The extended class name.
   * */
  get name() {
    return this.constructor.name;
  }
}

/**
 * Custom Error class for incorrect client request
 * @extends ApplicationError
 */
class BadRequestError extends ApplicationError {
  /**
   * Create error with specified message and status code
   * @param {string} message - The message value
   */
  constructor(message = 'Bad Request') {
    super(message);
    this.statusCode = 400;
  }
}

/**
 * Custom Error class for unauthorized requests
 * @extends ApplicationError
 */
class UnathorizedError extends ApplicationError {
  /**
   * Create error with specified message and status code
   * @param {string} message - The message value
   */
  constructor(message = 'Unathorized user') {
    super(message);
    this.statusCode = 401;
  }
}

/**
 * Custom Error class for read/write DB requests
 * @extends BadRequestError
 */
class DatabaseError extends BadRequestError {
  /**
   * Create error with specified message and status code
   * @param {string} message - The message value
   * */
  constructor(message = 'Database Request Error') {
    super(message);
  }
}

module.exports = {
  BadRequestError,
  UnathorizedError,
  DatabaseError,
};
