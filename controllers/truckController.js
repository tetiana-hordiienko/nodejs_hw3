const daoTruck = require('../models/dao/daoTruck');
const {BadRequestError} = require('../models/errorModel');
const {
  isUserTruck,
  isTruckAssigned,
  truckAssignedToUser,
  userHasAssignedTruck,
} = require('../routers/helpers/checksHelper');

const getUserTrucks = async (req, res) => {
  const trucks = await daoTruck.getAllTrucks(req.user);
  res.json({trucks});
};

const addUserTruck = async (req, res) => {
  const {type} = req.body;
  await daoTruck.addTruck(req.user['_id'], type);
  res.json({message: 'Truck created successfully!'});
};

const getUserTruckById = async (req, res) => {
  const truckId = req.params.id;
  const truck = await daoTruck.findTruckById(truckId, req.user['_id']);
  res.json({truck});
};

const updateUserTruckById = async (req, res) => {
  const truckId = req.params.id;
  const truck = await daoTruck.findTruckById(truckId, req.user['_id']);
  if (isTruckAssigned(truck)) {
    throw new BadRequestError('Cannot change details for assigned truck!');
  }
  const {type} = req.body;
  await daoTruck.changeTruckInfo(truckId, type);
  res.json({message: 'Truck details changed successfully'});
};

const assignUserTruckById = async (req, res) => {
  const truckId = req.params.id;
  const user = req.user;
  if (!(await isUserTruck(truckId, user['_id']))) {
    throw new BadRequestError('Only your truck can be assigned!');
  }
  const trucks = await daoTruck.getAllTrucks(user);
  if (userHasAssignedTruck(trucks)) {
    throw new BadRequestError(`Only one truck can be assigned in a moment!`);
  }
  await daoTruck.assignTruckToUser(truckId, user['_id']);
  res.json({message: 'Truck assigned successfully'});
};

const deleteUserTruckById = async (req, res) => {
  const truckId = req.params.id;
  const user = req.user;
  if (!(await isUserTruck(truckId, user['_id']))) {
    throw new BadRequestError('Only your truck can be deleted!');
  }
  const truck = await daoTruck.findTruckById(truckId, user['_id']);
  if (truckAssignedToUser(truck, user)) {
    throw new BadRequestError(`Assigned truck can't be deleted!`);
  }
  await daoTruck.deleteTruckById(truckId);
  res.json({message: 'Truck deleted successfully!'});
};

module.exports = {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTruckById,
  assignUserTruckById,
  deleteUserTruckById,
};
