const config = require('config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
// const generator = require('generate-password');

const JWT_SECRET = config.get('JWT_SECRET');
const {User} = require('../models/userModel');
// const {BadRequestError, DatabaseError} = require('../models/errorModel');
const {BadRequestError} = require('../models/errorModel');
const daoUser = require('../models/dao/daoUser');
// const {sendMailToUser} = require('../routers/helpers/mailerHelper');

const createProfile = async (req, res) => {
  const {email, password, role} = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.json({message: 'User created successfully!'});
};

const login = async (req, res) => {
  const WRONG_CRED_MESSAGE = 'Invalid email or password!';
  const {email, password} = req.body;
  const user = await daoUser.findUserByEmail(email, WRONG_CRED_MESSAGE);
  if (!(await bcrypt.compare(password, user.password))) {
    throw new BadRequestError(WRONG_CRED_MESSAGE);
  }

  const token = jwt.sign(
      {email, _id: user['_id'], role: user.role},
      JWT_SECRET,
  );

  res.json({message: 'Success', jwt_token: token});
};

const forgotPassword = async (req, res) => {
  // const {email} = req.body;
  // const user = await daoUser.findUserByEmail(email);
  // console.log('user id: ', user['_id']);
  // // const newPassword = Math.random().toString(36).slice(2);
  // const newPassword = generator.generate({
  //   length: 10,
  //   numbers: true,
  //   symbols: true,
  // });
  //
  // console.log('newPassword', newPassword);
  //
  // const hashed = await bcrypt.hash(newPassword, 10);
  //
  // await User.findById(
  //     user['_id'],
  //     {password: hashed},
  //     (err, user) => {
  //       if (err) {
  //         throw new DatabaseError('Update user password failed!');
  //       }
  //       user.password = hashed;
  //       user.save();
  //     },
  // );
  //
  // const data = {
  //   to: email,
  //   subject: 'Your new password',
  //   html: `Your new password ${newPassword} has been set.`
  // }
  // await sendMailToUser(data);

  res.json({message: 'New password sent to your email address!'});
};

module.exports = {
  createProfile,
  login,
  forgotPassword,
};
