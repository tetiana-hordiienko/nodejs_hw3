const daoUser = require('../models/dao/daoUser');
const bcrypt = require('bcryptjs');
const {DatabaseError, BadRequestError} = require('../models/errorModel');
const {User} = require('../models/userModel');
const {isDriver} = require('../routers/helpers/checksHelper');

const getProfileInfo = async (req, res) => {
  const _id = req.user['_id'];
  const {email, createdDate} = await daoUser.findUserById(_id);
  res.json({user: {_id, email, createdDate}});
};

const changeProfilePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const _id = req.user['_id'];
  const user = await daoUser.findUserById(_id);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new BadRequestError('Wrong password!');
  }
  const hashed = await bcrypt.hash(newPassword, 10);
  await User.findById(
      _id,
      {password: hashed},
      (err, user) => {
        if (err) {
          throw new DatabaseError('Update failed!');
        }
        user.password = hashed;
        user.save();
      },
  );
  res.json({message: 'Password changed successfully!'});
};

const deleteProfile = async (req, res) => {
  if (isDriver(req.user)) {
    throw new BadRequestError('Forbidden to delete driver profile!');
  }
  const {_id} = req.user;
  await daoUser.deleteUserById(_id);
  res.json({message: 'Profile deleted successfully!'});
};

module.exports = {
  getProfileInfo,
  changeProfilePassword,
  deleteProfile,
};
