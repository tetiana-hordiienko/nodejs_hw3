const daoLoad = require('../models/dao/daoLoad');
const daoTruck = require('../models/dao/daoTruck');
const {BadRequestError} = require('../models/errorModel');
const {ROLE, LOAD_STATUSES, LOAD_STATES, iterateState} =
    require('../globalConstants');
const {findSuitableTruck, findTruckToShip} = require('../models/dao/daoTruck');

const getUserLoads = async (req, res) => {
  const user = req.user;
  const role = user.role;
  const {status, limit = 10, offset = 0} = req.query;

  const requestOptions = {
    skip: parseInt(offset),
    limit: limit > 100 ? 10 : parseInt(limit),
  };

  let filterValue = '';

  if (!status) {
    filterValue = role === ROLE.shipper ?
                  `${LOAD_STATUSES.join('|')}` :
                  `${LOAD_STATUSES.slice(2).join('|')}`;
  }

  const filter = {status: {$regex: new RegExp(`${status || filterValue}`)}};

  const fieldToMatch = role === ROLE.shipper ? 'created_by' : 'assigned_to';
  filter[fieldToMatch] = user['_id'];
  const loads = await daoLoad.getAllLoads(filter, requestOptions);
  res.json({loads});
};

const addUserLoad = async (req, res) => {
  await daoLoad.addLoad(req.user['_id'], req.body);
  res.json({message: 'Load created successfully'});
};

const getUserActiveLoad = async (req, res) => {
  const load = await daoLoad.findActiveLoad(req.user._id) || 'No active loads';
  res.json({load});
};

const postUserLoadById = async (req, res) => {
  const loadId = req.params.id;
  const status = 'POSTED'; // здесь заменить на iterateStatus

  const load = await daoLoad.findAndPostLoad(req.user._id, loadId, status);

  const suitableTypeTruck = await findSuitableTruck(load);
  const truck = await findTruckToShip(suitableTypeTruck);

  if (!truck) {
    await daoLoad.updateLoad(
        load,
        {status: 'NEW'}, // здесь заменить на rollbackStatus
        [
          'Driver not found',
          'Load status rolled back to NEW',
        ],
    );

    return res.json({
      message: 'Driver not found! Load can be posted later again',
    });
  }

  const driverId = truck['assigned_to'];
  await daoLoad.updateLoad(
      load,
      {
        status: 'ASSIGNED',
        assigned_to: driverId,
        state: LOAD_STATES[0],
      },
      [
        `Load assigned to driver with id ${driverId}`,
        `Load state: En route to Pick Up`,
      ],
  );

  res.json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

const iterateLoadState = async (req, res) => {
  const activeLoad = await daoLoad.findActiveLoad(req.user._id);
  const newState = iterateState(LOAD_STATES, activeLoad.state);

  await daoLoad.updateLoad(
      activeLoad,
      {state: newState},
      [`Load state: ${newState}`],
  );

  if (activeLoad.state === LOAD_STATES[LOAD_STATES.length-1]) {
    const shipped = 'SHIPPED';
    await daoLoad.updateLoad(
        activeLoad,
        {status: shipped},
        [`Load status changed to ${shipped}`],
    );

    const truck = await daoTruck.findTruckByDriverId(activeLoad.assigned_to);
    await daoTruck.updateTruck(truck, {status: 'IS'});
  }

  res.json({message: `Load state changed to ${newState}`});
};

const getUserLoadById = async (req, res) => {
  const load = await daoLoad.findLoadById(req.params.id);

  if (load['created_by'] !== req.user._id) {
    throw new BadRequestError('No rights to interact with the load!');
  }

  res.json({load})
};

const updateUserLoadById = async (req, res) => {
  const load = await daoLoad.findLoadById(req.params.id);
  daoLoad.ensureUserLoad(load, req.user._id);
  daoLoad.ensureNewLoadStatus(load);

  const updates = req.body;

  await daoLoad.updateLoad(load, updates, ['Load details was changed']);

  res.json({message: 'Load details changed successfully'});
};

const deleteUserLoadById = async (req, res) => {
  const loadId = req.params.id;
  const load = await daoLoad.findLoadById(loadId);
  daoLoad.ensureUserLoad(load, req.user._id);
  daoLoad.ensureNewLoadStatus(load);
  await daoLoad.deleteLoadById(loadId);
  res.json({message: 'Load deleted successfully'});
};

const getUserLoadShippingDetailsById = async (req, res) => {
  const loadId = req.params.id;
  const load = await daoLoad.findLoadById(loadId);
  daoLoad.ensureUserLoad(load, req.user._id);

  const truck = await daoTruck.findTruckByDriverId(load.assigned_to);

  res.json({load, truck});
};

module.exports = {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  postUserLoadById,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  getUserLoadShippingDetailsById,
};
